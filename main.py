from aiogram import Bot, Dispatcher, executor, types
from aiogram.types.message import ContentType
import aiohttp
from lxml import html

from data import Data
from constants import *

from api_token import API_TOKEN

data = Data.get()

bot = Bot(token=API_TOKEN)
dp = Dispatcher(bot)

global session
session = None

async def scrape_bio(starting_username, skip=[]):
    global session
    async with session.get('https://t.me/'+starting_username) as resp:
        assert resp.status == 200
        text = await resp.text()
        tree = html.fromstring(text)
        usernames = tree.xpath('//div[@class="tgme_page_description"]/a[starts-with(@href, "https://t.me/")]/text()')
        print('usernames='+str(usernames))
        async for username in usernames:
            if username in skip:
                print("Recursion detected in the loop at "+username+" from @"+starting_username)
                continue
            if username.lower() == "@bio_chain_2":
                print("Found end of chain!")
                return [starting_username]
            else:
                print("not end of chain: "+username+", came from @"+starting_username)
                recurse = await scrape_bio(username[1:], skip+["@"+starting_username])
                if len(recurse) > 0:
                    return [starting_username] + recurse
        return []

@dp.message_handler(content_types=ContentType.NEW_CHAT_MEMBERS)
async def new_member(message: types.Message):
    if message.chat.id == ADMISSION_ID:
        new_member_admission(message)
    elif message.chat.id == MAIN_ID:
        new_member_chain(message)
    else:
        print("Skipping message from "+str(message.chat.id))
async def new_member_admission(message: types.Message):
    print(message)
    for member in message.new_chat_members:
        print("Adding member: "+str(member.id)+"; username="+member.username)
        if member.username != "":
            print("Telling member to get a username!")
            await message.reply(GET_USERNAME)
async def new_member_main(message: types.Message):
    for member in message.new_chat_members:
        print("Adding member: "+str(member.id)+"; username="+member.username)
        if member.username != "":
            print("Invite link leaked!")
            await message.reply(INVITE_LEAK)
        else:
            data.data['users'] += [{"id": member.id, "username": member.username}]

@dp.message_handler(content_types=ContentType.LEFT_CHAT_MEMBER)
async def left_member(message: types.Message):
    print(message)
    print("Someone left, strategic analysis...")

@dp.message_handler(commands=['chain', 'update'])
async def update_command(message: types.Message):
    print(message)
    if message.chat.id == MAIN_ID or message.chat.id == BOT_ID:
        x = await update_chain()
        print(x)
        await bot.send_message(BOT_ID, "Chain length: "+str(len(x))+"\n\nChain:\n\n"+" ⇒ ".join(x))
        await message.reply("Sent in bot control group")
    else:
        print("ignoring command")

async def update_chain():
    global session
    if session == None:
        session = aiohttp.ClientSession()
    print("start chain update!")
    userstmp = [x["username"] for x in data.data["users"]]
    print(userstmp)
    usersdone = []
    lastuserstmp = len(userstmp)
    while len(userstmp) > 0:
        print(userstmp)
        print(usersdone)
        user = userstmp[-1]
        print(user)
        x = await scrape_bio(user)
        if len(x) > 0:
            userlist = x #-1 because we append users, so the most recent is probably the last
            usersdone += userlist
        userstmp = [e for e in userstmp if e not in usersdone]
        if len(userstmp) == lastuserstmp:
            del userstmp[-1]
        else:
            lastuserstmp = len(userstmp)
    print("tip of chain is "+user)
    return userlist
@dp.message_handler(content_types=ContentType.ANY)
async def generic_message(message: types.Message):
#    print(message)
#    print(message["from"])
    if message.chat.id == MAIN_ID and message["from"]["username"] == "":
        await message.reply(BANNED_USERNAME)
        #message.chat.kick(message["from"].id, None)

if __name__ == '__main__':
    executor.start_polling(dp, skip_updates=True)


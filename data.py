import atexit, json

class Data():
    @classmethod
    def get(c):
        self = c()
        self.data = json.load(open("data.json", "r"))
        atexit.register(self.exit)
        return self
    def exit(self):
        json.dump(self.data, open("data.json", "w"))

